import React from 'react';
import './App.css';
import SynonymModal from "./components/SynonymModal/SynonymModal";

function App() {
  return (
    <div className="App">
      <SynonymModal />
    </div>
  );
}

export default App;
