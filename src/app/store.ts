import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import synonymReducer from '../components/SynonymModal/synonymController';

export const store = configureStore({
  reducer: {
    synonym: synonymReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
