/* eslint-disable no-unused-vars */
import React, {ReactElement} from 'react';
import styled from 'styled-components';

interface ContainerProps {
    backgroundColor: string;
}

const Container = styled.div<ContainerProps>`
  height: 14px;
  padding: 8px 12px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  font-weight: bold;
  background-color: ${props => props.backgroundColor || "#344273"};
  border: 1px solid ${props => props.backgroundColor || "#344273"};
  border-radius: 3px;
  color: #ffffff;
  &:hover {
    background-color: #fff;
    border: 1px solid #344273;
    color: ${props => props.backgroundColor || "#344273"};
  }
`;

const ContainerInactive = styled.div`
  height: 14px;
  padding: 8px 12px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: bold;
  background-color: rgba(52, 66, 115, 0.5);
  border: rgba(52, 66, 115, 0.5);
  border-radius: 3px;
  color: #ffffff;
`;


export enum ButtonColor {
    PURPLE= '#344273',
    GREEN= '#5E9574',
    BROWN= '#955E5E',
}

interface Props{
    text: string;
    color: ButtonColor;
    onClick: () => void;
    inactive: boolean;
}

const Button = ({text, color, inactive, onClick}: Props): ReactElement => {

    if (inactive) return <ContainerInactive>{text}</ContainerInactive>
    return (
        <Container backgroundColor={color}
                   onClick={onClick}
        >
            {text}
        </Container>
    )
}

export default Button;

