import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

export interface SynonymsState {
  synonyms: string[];
}

const initialState: SynonymsState = {
  synonyms: [],
};

export const allSynonyms = (state: RootState) => state.synonym.synonyms;

export const synonymController = createSlice({
  name: 'synonym',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    loadAllSynonyms: (state) => {
      const loadSynonyms = localStorage.getItem('synonyms');
      if (loadSynonyms) {
        state.synonyms = JSON.parse(loadSynonyms);
      }
    },
    saveAllSynonyms: (state) => {
      localStorage.setItem('synonyms', JSON.stringify(state.synonyms));
    },
    addNewSynonym: (state, action:PayloadAction<string> ) => {
      state.synonyms = [action.payload, ...state.synonyms];
    },
    resetSynonyms: (state, action:PayloadAction<string[]> ) => {
      state.synonyms = action.payload;
    },
  },
});

export const { addNewSynonym, loadAllSynonyms, resetSynonyms, saveAllSynonyms  } = synonymController.actions;

export default synonymController.reducer;
