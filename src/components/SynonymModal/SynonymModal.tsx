import React, {ReactElement, useEffect, useState} from 'react';
import styled from 'styled-components';
import Button, {ButtonColor} from "../Button/Button";
import {useAppDispatch, useAppSelector} from "../../app/hooks";
import {addNewSynonym, allSynonyms, loadAllSynonyms, resetSynonyms, saveAllSynonyms} from "./synonymController";
import Icon from "../Icon/Icon";


const Container = styled.div`
  width: 520px;
  height: 520px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  background: #EFEFEF;
  box-shadow: 0px 20px 50px rgba(0, 0, 0, 0.25);
  @media (max-width: 600px) {
    box-shadow: none;
    border: none;
    width: 100%;
    height: 100%;
  }
`;

const EditFooter = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 5px 0 10px 0;
`;

const FlexBlock = styled.div`
  display: flex;
  flex-direction: row;
  height: 31px;
`;

const GreyBoldLine = styled.div`
  border-bottom: 2px solid #DEDEDE;
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 30px;
`;

const HeaderText = styled.div`
  display: flex;
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  line-height: 28px;
`;

interface InputProps {
    hasValue: boolean;
}

const Input = styled.div<InputProps>`
  display: flex;
  flex-direction: column;
  .input {
    border: none;
    background-color: transparent;
    font-family: Circle, sans-serif !important;
    font-weight: normal;
    border-bottom: #344273 1px solid;
    font-size: 14px;
    color: ${props => props.hasValue ? '#000' : '#344273'};
    opacity: ${props => props.hasValue ? 1 : 0.5};
    height: 34px;
  }

  .input:focus {
    outline: none;
    opacity: 1;
  }
`;

const InputTitle = styled.div`
  color: #787878;
  font-family: Circe, sans-serif;
  font-size: 14px;
`;

const List = styled.div`
  display: flex;
  flex-direction: column;
  overflow: auto;
  flex: 1;
  margin: 6px 0;
`;

interface MarginProps {
    margin: string;
}

const Margin = styled.div<MarginProps>`
  margin: ${props => props.margin || 0};
`;

const Name = styled.div`
  opacity: 0.5;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  background-color: #ffffff;
  height: 40px;
  flex-shrink: 0;
  border-radius: 3px;
  margin-bottom: 5px;
  font-size: 16px;
  font-weight: bold;
  padding: 0 12px 0 11px;
`;

const Title = styled.div`
  color: #4F4F4F;
  display: flex;
  align-items: center;
  margin: 20px 0 0 0;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
`;

const SynonymModal = (): ReactElement => {
    const synonyms = useAppSelector(allSynonyms);
    const [inputValue, setInputValue] = useState('');
    const dispatch = useAppDispatch();
    const [editText, setEditText] = useState('');
    const [isEditing, setEditing] = useState(undefined as number | undefined);

    useEffect(() => {
        dispatch(loadAllSynonyms());
    }, [])

    const handleAdd = () => {
        dispatch(addNewSynonym(inputValue))
        setInputValue('');
    }

    const handleEdit = () => {
        if (isEditing !== undefined && editText) {
            const newSynonyms = [...synonyms];
            newSynonyms[isEditing] = editText;
            dispatch(resetSynonyms(newSynonyms))
        }
        setEditText('');
        setEditing(undefined);
    }

    const handleDelete = (index: number) => {
        const newSynonyms = [...synonyms]
        newSynonyms.splice(index, 1);
        dispatch(resetSynonyms(newSynonyms))
    }

    const clearAllSynonym = () => {
        dispatch(resetSynonyms([]))
    }

    const renderAddInput = () => {
        return (
            <Input hasValue={!!inputValue}>
                <InputTitle>
                    добавление синонима:
                </InputTitle>
                <input type="text"
                       name="addSynonym"
                       value={inputValue}
                       className="input"
                       placeholder="Введите название"
                       onChange={(e) => setInputValue(e.target.value)}/>
                <EditFooter>
                    <Button text="добавить"
                            color={ButtonColor.PURPLE}
                            onClick={() => handleAdd()}
                            inactive={inputValue === ''}
                    />
                </EditFooter>
            </Input>
        )
    }

    const renderEditInput = () => {
        return (
            <Input hasValue={!!editText}>
                <InputTitle>
                    редактирование синонима:
                </InputTitle>
                <input type="text"
                       name="editSynonym"
                       value={editText}
                       className="input"
                       placeholder="Введите название"
                       onChange={(e) => setEditText(e.target.value)}/>
                <EditFooter>
                    <Button text="сохранить"
                            color={ButtonColor.PURPLE}
                            onClick={() => handleEdit()}
                            inactive={editText === ''}
                    />
                    <Margin margin="0 0 0 10px" />
                    <Icon iconUrl="icons/BlackCross.svg"
                          size={[17, 17]}
                          onClick={() => {
                              setEditText('');
                              setEditing(undefined);
                          }}
                    />
                </EditFooter>
            </Input>
        )
    }

    const renderItems = (): ReactElement[] => {
        return synonyms.map((item: string, key) => {
            if (key === isEditing) {
                return renderEditInput();
            }
            return (
                <Row key={key}>
                    <Name>{item}</Name>
                    <Margin margin="0 0 0 auto"/>
                    <Icon iconUrl="icons/Edit.svg" size={[14, 14]}
                          onClick={() => {
                              setEditText(item);
                              setEditing(key);
                          }}/>
                    <Margin margin="0 0 0 10px"/>
                    <Icon iconUrl="icons/Delete.svg" size={[14, 14]} onClick={() => handleDelete(key)}/>
                </Row>
            )
        })
    }

    return (
        <Container>
            <Header>
                <HeaderText>
                    Редактирование группы синонимов поисковых фраз
                </HeaderText>
                <Icon iconUrl="icons/BlackCross.svg"
                      size={[17, 17]}
                      onClick={() => console.log('close')}
                      margin="0 2px 10px 0;"
                />
            </Header>
            <Margin margin="6px 0 0 0"/>
            <GreyBoldLine/>
            <Title>
                Синонимы
                <Margin margin="0 0 0 10px" />
                <Icon iconUrl="icons/Info.svg" size={[14, 14]} onClick={() => console.log('show info')}/>
            </Title>
            <List>
                {renderAddInput()}
                {renderItems()}
            </List>
            <GreyBoldLine/>
            <Margin margin="18px 0 0 0"/>
            <FlexBlock>
                <Button text="сохранить изменения"
                        color={ButtonColor.GREEN}
                        onClick={() => dispatch(saveAllSynonyms())}
                        inactive={false}
                />
                <Margin margin="0 0 0 18px"/>
                <Button text="очистить синонимы"
                        color={ButtonColor.BROWN}
                        onClick={() => clearAllSynonym()}
                        inactive={false}
                />
            </FlexBlock>
        </Container>
    )
}

export default SynonymModal;

