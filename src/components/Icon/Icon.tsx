import React from 'react';
import styled from "styled-components";

interface props{
    iconUrl: string;
    onClick: () => void
    size: number[];
    margin?: string;
}

interface IconStyleProps{
    height: number;
    width: number;
    url: string;
    margin?: string;
}

const IconStyle = styled.div<IconStyleProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  flex-shrink: 0;
  margin: ${({margin}) => margin || 0};
  height: ${({height}) => height}px;
  width: ${({width}) => width}px;
  background: url(${({url}) => url}) no-repeat center;
  opacity: 0.5;
  &:hover {
    opacity: 1;
  }
`;

const Icon = ({iconUrl, size, margin, onClick}: props) => {
    return <IconStyle height={size[0]} width={size[1]} url={iconUrl} onClick={onClick} margin={margin}/>
}

export default Icon
